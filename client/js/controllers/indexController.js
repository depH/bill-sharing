angular
  .module('app')
  .controller('indexController', ['$scope', 'Sample', function($scope, Sample) {
    $scope.name = "World";
    $scope.sayHello = function() {
      Sample.status().$promise
        .then(function (res) {
          $scope.response = res.data;
        })
    };
  }])
  .controller('groupController', ['$scope', 'Group', function($scope, Group) {
    Group.find().$promise
      .then(function (groups) {
        $scope.groups = groups;
      })
  }])
  .controller('billController', ['$scope', 'Account', 'Group', 'Bill', function($scope, Account, Group, Bill) {
    Account.find().$promise
      .then(function (groups) {
        $scope.accounts = groups;
      });

    Group.find().$promise
      .then(function (groups) {
        $scope.groups = groups;
      });
    $scope.submit = function () {
      console.log("submit invoked");
      $scope.bill.people = $scope.bill.share;

      console.log("submit invoked", $scope.bill);
      Bill.create($scope.bill).$promise
        .then(function(res){
          alert("Bill submitted")
        })
        .catch(function(err){
          console.log("error occured.");
        })
    }
  }]);
