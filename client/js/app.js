// Copyright IBM Corp. 2015. All Rights Reserved.
// Node module: loopback-getting-started-intermediate
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

angular
  .module('app', [
    'ngResource',
    'ui.router',
    'lbServices'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
      $urlRouterProvider) {
      $stateProvider
        .state('root', {
          url: '/',
          controller: 'indexController'
        })
        .state('list', {
          url: '/list',
          controller: 'listController'
        })
        .state('group', {
          url: '/group',
          controller: 'groupController',
          templateUrl: 'templates/group.html'
        })
        .state('bill', {
          url: '/bill',
          controller: 'billController',
          templateUrl: 'templates/bill.html'
        });

    $urlRouterProvider.otherwise('/');
  }]);
