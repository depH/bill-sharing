'use strict';

module.exports = function(Account) {

  // Account.register = function (data, cb) {
  //   if(!data) cb("details missing.");
  //
  //   let accountData = {
  //     type: 3,
  //     name: data.name,
  //     password: data.password ? data.password : data.phone.toString(),
  //     email: data.email,
  //     phone: data.phone
  //   };
  //
  //   Account.create(accountData)
  //     .then(function (user) {
  //       data.accountId = user.id;
  //       return Account.app.models.Detail.create(data)
  //     })
  //     .then(function (accDetail) {
  //       cb(null, handler.Success({message: "Account created successfully", data: {id: accDetail.accountId}}));
  //     })
  //     .catch(function (err) {
  //       cb(handler.Error(err.message))
  //     })
  // };
  //
  // Account.Map = function (id, vendorId, tripId, cb) {
  //
  //   let Trip = Account.app.models.Trip;
  //   let AssignedTrip = Account.app.models.AssignedTrip;
  //   let state = Trip.state();
  //
  //   Promise.all([Account.findOne({where: {id: vendorId}}), Trip.findOne({where: {id: tripId}}), AssignedTrip.find({where: {accountId: vendorId, tripId: tripId}})])
  //     .spread((account, trip, assigned) => {
  //       if(!account) throw new Error("Vendor does not exists.");
  //
  //       if(!trip) throw new Error("Trip does not exists.");
  //
  //       if(assigned.length) throw new Error("Vendor already Mapped to this Trip.");
  //
  //       if((account.id !== vendorId) && (trip.id !== tripId)) throw new Error("Data does not match.");
  //
  //       AssignedTrip.create({accountId: vendorId, tripId: tripId, active: true})
  //         .then((res) => {
  //           return Trip.upsertWithWhere({id: tripId}, {state: state["MAPPED"]["code"]})
  //         })
  //         .then((res) => {
  //           cb(null, handler.Success({message: "Vendor successfully mapped to Trip"}));
  //         })
  //         .catch((err) => {
  //           cb(handler.Error(err.message))
  //         });
  //     })
  //     .catch((err) => {
  //       cb(handler.Error(err.message))
  //     });
  // };
  //
  // //TODO : Exclude Admin from this API
  // Account.List = function (id, cb) {
  //   Account.find({include: {relation: 'detail', scope: {fields: ['company']}}})
  //     .then((accounts) => {
  //       cb(null, handler.Success({message: "Accounts list", data: accounts}));
  //     })
  //     .catch((err) => {
  //       cb(handler.Error(err.message))
  //     })
  // };
  //
  // Account.Assigned = function (id, tripId, cb) {
  //   let AssignedTrip = Account.app.models.AssignedTrip;
  //   let filter = {
  //     relation: 'account',
  //     scope: {
  //       fields: ['firstname', 'lastname', 'email'],
  //       include: {
  //         relation: 'detail',
  //         scope: {
  //           fields: ['company']
  //         }
  //       }
  //     }
  //   };
  //   AssignedTrip.find({where: {tripId: tripId}, include: filter})
  //     .then((accounts) => {
  //       cb(null, handler.Success({message: `Account list belongs to trip Id ${tripId}`, data: accounts}));
  //     })
  //     .catch((err) => {
  //       cb(handler.Error(err.message))
  //     })
  // };
  //
  // /**
  //  * disables relation between the account and trip
  //  * @param id
  //  * @param vendorId
  //  * @param tripId
  //  * @param cb
  //  * @constructor
  //  */
  // Account.Unset = function (id, vendorId, tripId, cb) {
  //   let AssignedTrip = Account.app.models.AssignedTrip;
  //
  //   // TODO: handle relation status
  //   // AssignedTrip.upsertWithWhere({accountId: vendorId, tripId: tripId}, {status: statusObj[status]["code"]})
  //   AssignedTrip.upsertWithWhere({accountId: vendorId, tripId: tripId}, {active: false})
  //     .then((accounts) => {
  //       cb(null, handler.Success({message: `Account list belongs to trip Id ${tripId}`, data: accounts}));
  //     })
  //     .catch((err) => {
  //       cb(handler.Error(err.message))
  //     })
  // };
  //
  // Account.Lead = function (id, cb) {
  //   let Trip = Account.app.models.Trip;
  //   Trip.find({where: {state: 0}, include: ["quotes", "customer"]})
  //     .then((trips) => {
  //       cb(null, handler.Success({message: `Unmapped Trips`, data: trips}));
  //     })
  //     .catch((err) => {
  //       cb(handler.Error(err.message))
  //     });
  // };
  //
  // Account.remoteMethod(
  //   'Lead', {
  //     accepts: [
  //       {arg: 'id', type: 'number', required: true, description: 'Current user Id'}
  //     ],
  //     http: {
  //       path: '/lead',
  //       verb: 'post'
  //     },
  //     returns: {
  //       root: true,
  //       type: 'object'
  //     }
  //   }
  // );
  //
  // Account.remoteMethod(
  //   'Unset', {
  //     accepts: [
  //       {arg: 'id', type: 'number', required: true, description: 'Current user Id'},
  //       {arg: 'vendorId', type: 'number', required: true, description: 'Vendor Id'},
  //       {arg: 'tripId', type: 'number', required: true, description: 'Trip Id'}
  //     ],
  //     http: {
  //       path: '/unset',
  //       verb: 'post'
  //     },
  //     returns: {
  //       root: true,
  //       type: 'object'
  //     }
  //   }
  // );
  //
  //
  // Account.remoteMethod(
  //   'Assigned', {
  //     accepts: [
  //       {arg: 'id', type: 'number', required: true, description: 'Current user Id'},
  //       {arg: 'tripId', type: 'number', required: true, description: 'Trip Id'}
  //     ],
  //     http: {
  //       path: '/Assigned',
  //       verb: 'post'
  //     },
  //     returns: {
  //       root: true,
  //       type: 'object'
  //     }
  //   }
  // );
  //
  // Account.remoteMethod(
  //   'Map', {
  //     accepts: [
  //       {arg: 'id', type: 'number', required: true, description: 'Account Id'},
  //       {arg: 'vendorId', type: 'number', required: true, description: 'Vendor Id'},
  //       {arg: 'tripId', type: 'number', required: true, description: 'Trip Id'}
  //     ],
  //     http: {
  //       path: '/Map',
  //       verb: 'post'
  //     },
  //     returns: {
  //       root: true,
  //       type: 'object'
  //     }
  //   }
  // );
  //
  // Account.remoteMethod(
  //   'List', {
  //     accepts: [
  //       {arg: 'id', type: 'number', required: true, description: ''}
  //     ],
  //     http: {
  //       path: '/list',
  //       verb: 'get'
  //     },
  //     returns: {
  //       root: true
  //     }
  //   }
  // );
  //
  // Account.remoteMethod(
  //   'register', {
  //     accepts: {
  //       arg: 'data', type: 'object', description : ['{"company": "", "owner": "","website": "","period": 0,"employee": 0,"sales": 0,"destination": "","trips": 0,"turnover": 0,"prole": "","additional": "", "firstname": "", "lastname": "", "email": "", "phone": "", "password": ""}']
  //     },
  //     http: {
  //       path: '/register',
  //       verb: 'post'
  //     },
  //     returns: {
  //       root: true,
  //       type: 'object'
  //     }
  //   }
  // );
};
