'use strict';

module.exports = function(Sample) {


  Sample.status = function(cb) {
    var response = "Response from Backend";
    cb(null, response);
  };

  Sample.remoteMethod(
    'status', {
      http: {
        path: '/status',
        verb: 'get'
      },
      returns: {
        arg: 'data',
        type: 'string'
      }
    }
  );


};
