const env = process.env.NODE_ENV || 'development';
const async = require('async');

module.exports = function(app, cb) {
  if (env !== 'development') {
    return cb();
  }

  const dummyModels = ['User', 'Container'];         //Enter the name of model to skip from auto migration

  const appModels = Object.keys(app.models);

  for (let dummyModel of dummyModels) {
    const index = appModels.indexOf(dummyModel);
    if (index > -1 ) {
      appModels.splice(index, 1);
    }
  }

  const ds = app.dataSources.postgre;  //Update Datasource on Change of Db

  const asyncDone = () => cb();

  const migrateModel = (model, callback) =>
      ds.isActual(model, function(err, actual) {
        if (!actual) {
          console.log(`Auto updating ${model}`);
          return ds.autoupdate(model, function(err) {
            if (err) {
              throw err;
            }
            return callback();
          });
        } else {
          return callback(null);
        }
      });

  return async.eachSeries(appModels, migrateModel, asyncDone);
};
